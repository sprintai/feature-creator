config_file = 'configs/config.ini'
feature_config_file = 'configs/feature_config.csv'

client_config = 'client_config'
path_key = 'client_path'
bucket_name_key = 'bucket_name'
tenant_key = 'tenant'
output_path_key = 'output_path'

run_parameters_config = 'run_parameters'
run_date_key = 'run_date'
set_run_date_key = 'set_run_date'

input_section_config = 'input_properties'
feature_input_key_config = 'feature_input'

feature_section_config = 'feature_properties'
feature_list_key_config = 'primary_feature_list'

method_key_config = 'method'
rel_cal_key_config = 'rel_cal'
impute_method_key_config = 'impute_method'

ros_bucket = 'ros'
availability_bucket = 'availability'
sales_bucket = 'sales'

week_key = 'week'
month_key = 'month'



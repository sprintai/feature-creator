import pandas as pd


class Time:
    """
    Time Object Initialized with Date
    """

    def __init__(self, date, n_period_agg, hierarchy):
        self.date = pd.to_datetime(date)
        self.week = None
        self.month = None
        self.year = None
        self.n_period_agg= n_period_agg
        self.parse_date()
        self.hierarchy = hierarchy

    def parse_date(self):
        """
        Parses date to Week, Month, Year
        :return: None
        """
        self.week = self.date.week
        self.month = self.date.month
        self.year = self.date.year

    def get_start_date(self):
        """
        Get Start Date based on the Run Date
        :return: Start Date
        """

        start_date = self.date - pd.to_timedelta(self.date.weekday(), unit='d') - \
                            pd.to_timedelta((self.n_period_agg - 1) * 7, unit='d')
        print("Start_Date for Feature: ", start_date)

        return start_date

    def get_end_date(self):
        """
        Get End Date Based on the run_date
        :return: End Date
        """
        end_date = self.date + pd.to_timedelta((6 - self.date.weekday()), unit='d')
        print("End_Date for Feature: ", end_date)

        return end_date

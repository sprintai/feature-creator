from base.colnames import col_calc_feature_value
from base.config_params import config_file
from input.input_parser import get_value_from_config

import numpy as np


def get_reliable_data_availability(df):
    min_avl = get_value_from_config(config_file, 'availability_properties', 'min_avl')
    df.loc[df[col_calc_feature_value] < int(min_avl), col_calc_feature_value] = np.nan
    return df

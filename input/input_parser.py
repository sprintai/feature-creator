from configparser import ConfigParser


def get_list_from_config(input_file, section, key):
    """
    Parses Config File and returns list of values given by section and key
    :param input_file: Input Config File
    :param section: Section to be parsed (defined in Config)
    :param key: Key
    :return: List of Values
    """
    parser = ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})
    parser.read(input_file)

    return parser.getlist(section, key)


def get_value_from_config(input_file, section, key):
    """
    Parses Config File and returns value given by section and key
    :param input_file: Input Config File
    :param section: Section to be parsed (defined in Config)
    :param key: Key
    :return: Value
    """
    parser = ConfigParser()

    parser.read(input_file)
    return parser[section][key]

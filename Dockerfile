
FROM python:3.7-buster

WORKDIR /app

COPY . /app

RUN pip install --upgrade pip
RUN pip install pandas
RUN pip install numpy
RUN pip install boto3
RUN pip install datetime
RUN pip install pyarrow
RUN pip install requests
RUN pip install s3fs
RUN pip install xlrd
RUN pip install openpyxl

ENTRYPOINT ["python", "process.py"]


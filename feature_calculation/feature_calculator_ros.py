from base.core import Availability, get_data_from_date_range, Sales, \
    Ros, merge_and_add_relevant_columns
from feature_calculation.feature_calculator import FeatureCalculator
from base.colnames import col_calc_feature_value, col_ros
from base.input_params import technique_key_input


class FeatureCalculatorRateOfSale(FeatureCalculator):
    """

    """
    def __init__(self, feature, date):
        super().__init__(feature, date)
        self.sales = None
        self.daily_inventory = None

    def extract_data(self, daily_inventory, sales):
        """
        Extracts Daily Inventory Data for the given Time Hierarchy
        :param sales: Daily Sales File
        :return:
        """
        start_date, end_date = self.get_start_and_end_date()

        self.sales = get_data_from_date_range(sales, start_date, end_date)
        self.daily_inventory = get_data_from_date_range(daily_inventory, start_date, end_date)

    def get_base_calc_feature(self, product_master, store_master, size_ratio):
        """
        Calculates Base Features for Sales --> Simple
        :param product_master:
        :param store_master:
        :return: None
        """
        availability = Availability(self.daily_inventory, product_master)
        sales = Sales(self.sales, product_master)
        ros = Ros(sales, availability)

        print('Calculating Using Technique:', self.feature.calc_method[technique_key_input])
        if self.feature.calc_method[technique_key_input] == 'simple':

            self.base_df = ros.get_simple_ros(size_ratio)
            self.base_df.rename(columns={col_ros: col_calc_feature_value}, inplace=True)
            self.base_df = merge_and_add_relevant_columns(self.base_df, product_master, store_master)

    def calculate_feature(self, sales, daily_inventory, product_master, store_master, size_ratio):
        """
        Inherits calculate_feature from feature_calculator Class
        :param sales:
        :param daily_inventory:
        :param product_master:
        :param store_master:
        :param size_ratio:
        :return:
        """
        self.extract_data(sales, daily_inventory)
        self.get_base_calc_feature(product_master, store_master, size_ratio)

        if self.base_df is None:
            "No Calculations Done!"
            return None

        return super().calculate_feature()

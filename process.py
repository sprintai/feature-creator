import json
import sys
from ast import literal_eval
sys.path.insert(0, '')

import pandas as pd

import os
import boto3
s3_resource = boto3.resource("s3", region_name="ap-south-1")

from base.colnames import col_feature_id, col_dates_to_be_run, col_date, col_dates_created
from base.core import save_df_to_csv, add_date_to_feature_config
from base.config_params import config_file, input_section_config, feature_input_key_config, \
    ros_bucket, availability_bucket, sales_bucket, run_parameters_config, run_date_key, feature_config_file, \
    set_run_date_key, client_config, output_path_key
from feature_calculation.feature_calculator_availability import FeatureCalculatorAvailability
from feature_calculation.feature_calculator_ros import FeatureCalculatorRateOfSale
from feature_calculation.feature_calculator_sales import FeatureCalculatorSales

from base.input_params import feature_config_key_input
from input.input_parser import get_value_from_config

from input.feature import Feature
from input.dataframes import BaseDataFrames, InputDataFrames




def assign_bucket_to_feature(feature, date, base_df, daily_df):
    """
    Assigns Feature Creator Function on the basis of its Primary Feature
    :param feature: Feature Object
    :param date: Date to get week/month level for which feature needs to be created
    :param base_df: Object containing Base Files
    :param daily_df: Object containing Daily Files
    :return: Created Feature Value DataFrame
    """

    print('Calculating for Feature: ', feature.feature_id)
    print('Primary Feature: ', feature.primary_feature)
    print('LH: ', feature.location_hierarchy, ', PH: ', feature.product_hierarchy, ', TH: ', feature.time_hierarchy)
    output_path = get_value_from_config(config_file, client_config, output_path_key)

    if feature.primary_feature == ros_bucket:

        feature_calculator = FeatureCalculatorRateOfSale(feature, date)
        calc_feature = feature_calculator.calculate_feature(daily_df.daily_inventory, daily_df.daily_sales,
                                                            base_df.product_master, base_df.store_master,
                                                            base_df.size_ratio)
        success = save_df_to_csv(calc_feature, output_path, feature.feature_id, str(feature.n_period_agg))
        if success:
            add_date_to_feature_config(feature_calculator.start_date, feature.feature_id)

    elif feature.primary_feature == availability_bucket:

        feature_calculator = FeatureCalculatorAvailability(feature, date)
        calc_feature = feature_calculator.calculate_feature(daily_df.daily_inventory,
                                                            base_df.product_master, base_df.store_master,
                                                            base_df.size_ratio)
        success = save_df_to_csv(calc_feature, output_path, feature.feature_id, str(feature.n_period_agg))
        if success:
            add_date_to_feature_config(feature_calculator.start_date, feature.feature_id)

    elif feature.primary_feature == sales_bucket:

        feature_calculator = FeatureCalculatorSales(feature, date)
        calc_feature = feature_calculator.calculate_feature(daily_df.daily_sales,
                                                            base_df.product_master, base_df.store_master)
        success = save_df_to_csv(calc_feature, output_path, feature.feature_id, str(feature.n_period_agg))
        if success:
            add_date_to_feature_config(feature_calculator.start_date, feature.feature_id)

    else:
        raise Exception("Feature Calculator Not defined for primary feature: " + feature.primary_feature)


def set_dates_to_be_run(daily_df, feature_config):
    """
    Sets Time Hierarchy Values as Start Date (Monday) for which each Feature needs to be run.
    Eg- F1 -> TH-values -> Week - 1,2, Year 2019, returns F1 -> ['31-Dec-2018', '7-Jan-2019'] as list

    :param daily_df: daily_input files-> contains Daily-Sales and Daily-Inventory
    :param feature_config: Contains Run Dates Log for each feature
    :return: modified dates_config df with Dates_to_be_run column modified
    """
    print("Setting Dates to be run based on Daily Input Files...")
    daily_inv_dates = (
            daily_df.daily_inventory[col_date] - pd.to_timedelta(daily_df.daily_inventory[col_date].dt.weekday,
                                                                 unit='D'))
    daily_inv_dates = daily_inv_dates.drop_duplicates()
    daily_inv_dates = daily_inv_dates.dt.strftime('%Y-%m-%d')
    daily_sales_dates = (
            daily_df.daily_sales[col_date] - pd.to_timedelta(daily_df.daily_sales[col_date].dt.weekday,
                                                             unit='D'))
    daily_sales_dates = daily_sales_dates.drop_duplicates()
    daily_sales_dates = daily_sales_dates.dt.strftime('%Y-%m-%d')

    curr_dates = list(set(daily_inv_dates.unique()).union(set(daily_sales_dates.unique())))

    feature_config[col_dates_to_be_run] = feature_config[col_dates_created].apply(
        lambda x: sorted(list(set(curr_dates).difference(set(literal_eval(x))))))
    feature_config.to_csv(feature_config_file, index=False)

    return feature_config


def create_features_main():
    """
    Main Function, Runs For each Feature defined in json based on run_date defined in config and saves its value
    :return: None
    """
    feature_input_path = get_value_from_config(config_file, input_section_config, feature_input_key_config)

    with open(feature_input_path) as f:
        feature_input = json.load(f)

    feature_list = feature_input[feature_config_key_input]
    curr_date = get_value_from_config(config_file, run_parameters_config, run_date_key)
    print('Run Date: ', curr_date)
    base_df = BaseDataFrames()
    daily_df = InputDataFrames()
    feature_config_df = pd.read_csv(feature_config_file)
    set_run_date = get_value_from_config(config_file, run_parameters_config, set_run_date_key)

    if int(set_run_date) == 1:
        feature_config_df = set_dates_to_be_run(daily_df, feature_config_df)

    for feature_key in feature_list:
        feature = Feature(feature_key)
        try:
            run_dates_list = (feature_config_df.loc[feature_config_df[col_feature_id] ==
                                                    feature.feature_id, col_dates_to_be_run].iloc[0])
            for run_date in run_dates_list:
                assign_bucket_to_feature(feature, run_date, base_df, daily_df)
        except IndexError:
            print('run date not defined for Feature: ', feature.feature_id)

    try:
        bucket_name = "ist-tcns"  # s3 bucket name
        root_path = 'CalcFiles/'  # local folder for upload

        my_bucket = s3_resource.Bucket(bucket_name)

        for path, subdirs, files in os.walk(root_path):
            path = path.replace("\\", "/")
            directory_name = path.replace(root_path, "")
            for file in files:
                my_bucket.upload_file(os.path.join(path, file), "feature-creator-master/output/"+directory_name + '/' + file)

    except Exception as err:
        print(err)


if __name__ == '__main__':
    create_features_main()

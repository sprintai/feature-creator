col_avl = 'Available'
col_inv_qty = 'Inv_Qty'
col_date = 'Date'
col_store_code = 'Store_Code'
col_week = 'Week'
col_year = 'Year'

col_ag = "AG"
col_style_code = 'Style_Code'
col_sku = 'Sku_Code'
col_count_size = 'Count_Sizes'

col_size_ratio = 'Size_Ratio'

col_weighted_avl = 'WAVL'
col_simple_avl = 'SAVL'

col_uplift = 'Uplift'

col_calc_feature_value = 'Calc_Feature_Value'

col_ros = 'ROS'

col_primary_feature = "Primary_Feature"
col_technique = "Technique"
col_reliable = "Reliable"
col_impute_method = "Impute_Method"
col_agg_func = "Aggregate_Function"
col_n_period_agg = "n_periods_agg"

col_quantity = "Sales_Qty"

col_feature_id = "Feature_Id"
col_dates_created = "Dates_Created"
col_dates_to_be_run = "Dates_To_Be_Run"



col_start_date = 'Start_Date'

col_brand = "Brand"
col_season = "Season"
col_item_category = "Item Category"
col_product_group = "Product Group"
col_product_grade = "Product_Grade"




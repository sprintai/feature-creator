import os
from ast import literal_eval

from base.colnames import *
import pandas as pd
import numpy as np

from base.config_params import feature_config_file


class Availability:
    """
    Availability Object -> Used for Daily-Inventory File. Returns at Store_Code x Style_Code Level
    """

    def __init__(self, daily_inventory: pd.DataFrame, product_master: pd.DataFrame):
        temp_df = daily_inventory.copy()
        temp_df[col_avl] = 0
        temp_df.loc[temp_df[col_inv_qty] > 0, col_avl] = 1
        self.daily_inventory_processed = temp_df.merge(product_master, on=[col_sku], how='inner')

    def check_empty(self) -> bool:
        """
        Check if DataFrame is Empty
        :return: True or False
        """
        if self.daily_inventory_processed.empty:
            print('Daily-Inventory DataFrame empty!')
            return True
        return False

    def get_weighted_availability(self, size_ratio: pd.DataFrame):
        """
        Calculates Daily Weighted Availability at Style_Code Level --> WAVL = Avl * Size_Ratio
        :param size_ratio: Size_Ratio DataFrame
        :return: Weighted Availability DataFrame
        """
        if size_ratio.empty:
            print("Size Ratio DataFrame Empty! Cannot proceed further")
            return None
        size_ratio_levels = list(set(size_ratio.columns) - {col_size_ratio})
        weighted_avl = self.daily_inventory_processed.merge(size_ratio, on=size_ratio_levels, how='inner')
        weighted_avl[col_weighted_avl] = weighted_avl[col_size_ratio] * weighted_avl[col_avl]
        weighted_avl = weighted_avl.groupby([col_date, col_style_code, col_store_code])[
            col_weighted_avl].sum().reset_index()

        return weighted_avl

    def get_simple_availability(self):
        """
        Calculates Daily Simple Availability at Style_Code Level --> SAVL = sum(Avl)/Count_Sizes
        :return: Simple Availability DataFrame
        """
        simple_avl = self.daily_inventory_processed.groupby([col_date, col_style_code, col_store_code, col_count_size])[
            col_avl].sum().reset_index()
        simple_avl[col_simple_avl] = simple_avl[col_avl] / simple_avl[col_count_size]

        return simple_avl.groupby([col_date, col_style_code, col_store_code])[col_simple_avl].sum().reset_index()


def get_year(df):
    """
    Get Year on the basis of Week Number, Handles end year dates
    :param df: Base DataFrame
    """
    df[col_year] = df[col_date].dt.year
    df.loc[df[col_date] == '31 Dec 2018', col_year] = 2019
    df.loc[df[col_date].isin(['30 Dec 2019', '31 Dec 2019']), col_year] = 2020

    return df


class Sales:
    """
    Sales Object -> Used for Daily-Sales File. Returns at Store_Code x Style_Code Level
    """

    def __init__(self, daily_sales: pd.DataFrame, product_master: pd.DataFrame):
        self.daily_sales = daily_sales.merge(product_master[[col_sku,
                                                             col_style_code]].drop_duplicates(), on=[col_sku],
                                             how='inner')

    def check_empty(self) -> bool:
        """
        Check if DataFrame is Empty
        :return: True or False
        """
        if self.daily_sales.empty:
            print('Daily-Sales DataFrame empty!')
            return True
        return False

    def get_sales(self) -> pd.DataFrame:
        """
        Returns Sales df excluding returns. Technique --> SIMPLE
        :return: Sales DataFrame
        """
        daily_sales = self.daily_sales.loc[self.daily_sales[col_quantity] > 0]

        return daily_sales.groupby([col_date, col_store_code, col_style_code]
                                   )[col_quantity].sum().reset_index()

    def get_sales_including_returns(self) -> pd.DataFrame:
        """
        Returns Sales df including returns. Technique --> SIMPLE-RETURNS
        :return: Sales DataFrame
        """
        return self.daily_sales.groupby([col_date, col_store_code, col_style_code]
                                        )[col_quantity].sum().reset_index()

    def get_uplift_normalized_sales(self, uplift_df: pd.DataFrame) -> pd.DataFrame:
        """
        Get Simple Sales normalized by uplifts
        :param uplift_df: Uplift DataFrame at Store_Code x Style_Code level
        :return: Sales DataFrame
        """
        if uplift_df.empty:
            print("WARN: Empty Uplift DataFrame, using default uplift value as 1")
            return self.daily_sales

        temp_df = self.daily_sales.merge(uplift_df, on=[col_sku, col_store_code, col_date], how='left')
        temp_df[col_uplift].fillna(1, inplace=True)
        temp_df[col_quantity] = temp_df[col_quantity] * temp_df[col_uplift]
        temp_df.drop(columns={col_uplift}, inplace=True)

        return temp_df


def calculate_ros(avl_df: pd.DataFrame, sales_df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculates Weekly ROS by Sum of Sales/ Sum of Availability
    :param avl_df: Availability DataFrame
    :param sales_df: Sales DataFrame
    :return: ros DataFrame
    """
    ros = sales_df.merge(avl_df, on=[col_style_code, col_store_code, col_date], how='outer')
    ros[col_week] = ros[col_date].dt.week
    ros = get_year(ros)
    ros = ros.groupby([col_store_code, col_style_code, col_week, col_year]
                      ).agg({col_quantity: 'sum', col_weighted_avl: 'sum'}).reset_index()
    ros[col_ros] = ros[col_quantity] / ros[col_weighted_avl]

    return ros


class Ros:
    """
    ROS Object --> Using Sales and Availability Objects to create ROS at Store_Code x Style_Code level.
    This is already aggregated at Week Level
    """

    def __init__(self, sales: Sales, avl: Availability):
        self.sales = sales
        self.availability = avl


    def check_empty(self) -> bool:
        """
        Check if DataFrame is Empty
        :return: True or False
        """
        if self.availability.check_empty() or self.sales.check_empty():
            return True
        return False

    def get_simple_ros(self, size_ratio: pd.DataFrame) -> pd.DataFrame:
        """
        Return Simple ROS -> Simple Sales / WAVL
        :param size_ratio: Size_Ratio DataFrame
        :return: ROS DataFrame
        """
        avl_df = self.availability.get_weighted_availability(size_ratio)
        sales_df = self.sales.get_sales()
        ros = calculate_ros(avl_df, sales_df)

        return ros

    def get_uplift_normalized_ros(self, size_ratio: pd.DataFrame, uplift_df: pd.DataFrame) -> pd.DataFrame:
        """
        Return Uplift Normalized ROS -> Uplift Normalized Sales/ WAVL
        :param size_ratio: Size_Ratio DataFrame
        :param uplift_df: Uplift_Ratio DataFrame
        :return: ROS DataFrame
        """
        avl_df = self.availability.get_weighted_availability(size_ratio)
        sales_df = self.sales.get_uplift_normalized_sales(uplift_df)
        ros = calculate_ros(avl_df, sales_df)

        return ros


def get_data_from_date_range(df, start_date, end_date):
    """
    Returns Data Between a date range
    :param df:
    :param start_date:
    :param end_date:
    :return:
    """
    return df.loc[(df['Date'] >= start_date) & (df['Date'] < end_date)]


def merge_df_with_master(df: pd.DataFrame, master: pd.DataFrame, hierarchy: list):
    """
    :param df: DataFrame to be merged
    :param master: Master DataFrame to be merged with
    :param hierarchy: On Merge
    :return: Merged df
    """
    return df.merge(master, on=hierarchy, how='inner')


def get_save_path(df: pd.DataFrame, base_path: str) -> str:
    """

    :param df:
    :param base_path:
    """
    if df[col_year].nunique() > 1:
        print('Error in Time Hierarchy! Multiple Years Data Present', df[col_year].unique())
        return None
    if df[col_year].nunique() > 1:
        print('Error in Time Hierarchy! Multiple Weeks Data Present', df[col_year].unique())
        return None

    year_no = df[col_year].unique()[0]
    week_no = df[col_week].unique()[0]

    save_path = base_path + 'year=' + str(year_no) + '/week=' + str(week_no) + '/'

    return save_path


def save_df_to_csv(df: pd.DataFrame, base_path: str, feature_id: str, n_period_agg: str):
    """
    Saves df to Path defined. Name of File is combination of Feature_id and n_period_agg
    :param df:
    :param base_path:
    :param feature_id:
    :param n_period_agg:
    :return: None
    """
    if df is None:
        print("Not Saving!")
        return False

    save_path = get_save_path(df, base_path)

    if save_path is None:
        print("Not Saving!")
        return False

    if not os.path.exists(save_path):
        os.makedirs(save_path)

    df.to_csv(save_path + feature_id + '_' + n_period_agg + 'W.csv')

    print("Saving Feature as csv!")

    return True


def merge_and_add_relevant_columns(base_df, product_master, store_master):
    """
    Merges with Product Master, Store Master and adds week and year
    :param base_df:
    :param product_master:
    :param store_master:
    """
    product_master = product_master[[col_style_code, col_brand,\
                                     col_ag, col_season,col_item_category,col_product_group,
                                     col_product_grade]].drop_duplicates()
    base_df = merge_df_with_master(base_df, product_master, col_style_code)
    base_df = merge_df_with_master(base_df, store_master, col_store_code)

    if col_date in base_df.columns:
        base_df[col_week] = base_df[col_date].dt.week
        base_df = get_year(base_df)

    return base_df


def add_date_to_feature_config(run_date, feature_id):
    """
    Adds Run Date to Feature Config File
    :param run_date:
    :param feature_id:
    """
    feature_config = pd.read_csv(feature_config_file)

    dates_created = literal_eval(feature_config.loc[feature_config[col_feature_id] ==
                                        feature_id, col_dates_created].iloc[0])

    run_date = run_date.strftime('%Y-%m-%d')
    dates_created.append(run_date)
    feature_config.at[feature_config[col_feature_id] == feature_id, col_dates_created] = [dates_created]
    print(feature_config[col_dates_created])
    feature_config.to_csv(feature_config_file, index=False)

    print("Adding Run Date to Config...")

from base.core import get_data_from_date_range, Sales, \
    merge_and_add_relevant_columns
from feature_calculation.feature_calculator import FeatureCalculator
from base.colnames import col_calc_feature_value, col_quantity
from base.input_params import technique_key_input

"""
-Normal Sales
-Sales without Returns
-Uplift Normalized
-Discount Normalized

"""
class FeatureCalculatorSales(FeatureCalculator):

    def __init__(self, feature, date):
        super().__init__(feature, date)
        self.sales = None

    def extract_data(self, sales):
        """
        Extracts Daily Inventory Data for the given Time Hierarchy
        :param sales: Daily Sales File
        :return:
        """
        start_date, end_date = self.get_start_and_end_date()

        self.sales = get_data_from_date_range(sales, start_date, end_date)

    def get_base_calc_feature(self, product_master, store_master):
        """
        Calculates Base Features for Sales --> Simple
        :param product_master:
        :param store_master:
        :return: None
        """

        print('Calculating Using Technique:', self.feature.calc_method[technique_key_input])
        sales = Sales(self.sales, product_master)

        if sales.check_empty():
            self.base_df = None
            return

        if self.feature.calc_method[technique_key_input] == 'simple':
            self.base_df = sales.get_sales()
            self.base_df.rename(columns={col_quantity: col_calc_feature_value}, inplace=True)
            self.base_df = merge_and_add_relevant_columns(self.base_df, product_master, store_master)

    def calculate_feature(self, sales, product_master, store_master):
        """

        :param sales:
        :param product_master:
        :param store_master:
        :return:
        """
        self.extract_data(sales)
        self.get_base_calc_feature(product_master, store_master)

        return super().calculate_feature()

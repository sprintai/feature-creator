from base.colnames import *
from base.config_params import config_file, client_config, path_key, bucket_name_key, tenant_key
from input.input_parser import get_value_from_config
import pandas as pd

from datetime import timedelta
import pyarrow.parquet as pq
import s3fs
import os
s3 = s3fs.S3FileSystem()

def download_directory(bucket_name, s3_folder_path, dest_path):
    import boto3

    client = boto3.client('s3')
    resource = boto3.resource('s3')
    try:
        print('s3 folder path :', s3_folder_path)
        print('Local Destination folder :', dest_path)
        response = client.list_objects(Bucket=bucket_name, Prefix=s3_folder_path)
        #print("response : ", response)
        for content in response.get('Contents', []):
            #print('file: ', content.get('Key'))
            if s3_folder_path[-1:] != "/":
                s3_folder_path = s3_folder_path + "/"

            file_dest_path = os.path.join(dest_path, content.get('Key').replace(s3_folder_path, ""))
            print('file found : ', content.get('Key'))
            if s3_folder_path == content.get('Key'):
                print('Empty file found')
                continue
            if not os.path.exists(os.path.dirname(file_dest_path)):
                os.makedirs(os.path.dirname(file_dest_path))
                print('Created directory :', file_dest_path)
            print('Downloading file at : ', file_dest_path)
            resource.meta.client.download_file(bucket_name, content.get('Key'), file_dest_path)
            print('Download Complete')
    except Exception as e:
        print("ERROR : " + str(e))
        return False, e
    return True, "Downloaded successfully!"

def get_s3_path_from_date(bucket_name, tenant, file_type, current_date):
    return 's3://'+bucket_name+'/'+tenant+'/'+file_type+'/Year='+str(current_date.year)+'/Month='+str(current_date.month)+'/Day_of_Month='+str(current_date.day)

def get_data_from_date_range(bucket_name, tenant, file_type, start_date, end_date):
    delta = end_date - start_date       # as timedelta
    data = pd.DataFrame()

    for i in range(delta.days + 1):
        current_date = start_date + timedelta(days=i)
        #print(current_date.day, current_date.month, current_date.year)
        try:
            file_path = get_s3_path_from_date(bucket_name, tenant, file_type, current_date)
            current_df = pq.ParquetDataset(file_path, filesystem=s3).read_pandas().to_pandas()
            data = data.append(current_df,ignore_index=True)
        except Exception as e:
            print(e)
        #print(file_path)
    #print(counter)

    return data

def get_input_path() -> str:
    """
    Returns Input Path for the Client Files
    :return: path
    """
    base_path = get_value_from_config(config_file, client_config, path_key)
    bucket_name = get_value_from_config(config_file, client_config, bucket_name_key)
    tenant = get_value_from_config(config_file, client_config, tenant_key)

    path = base_path + bucket_name + tenant
    return path


class BaseDataFrames:
    """
    Contains Base DataFrame Objects --> Product_Master, Store_Master, Size_Ratio, Uplift_Map
    """
    def __init__(self):
        #self.input_path = get_input_path()
        download_directory('ist-tcns', 'feature-creator-master', 'BaseFiles')
        print('Reading Base Files...')
        directory = 'BaseFiles/'

        self.product_master = pd.read_parquet(directory + 'Product_Master.parquet')
        self.product_master.rename(columns = {'Material': col_sku}, inplace=True)
        self.store_master = pd.read_csv(directory + 'TCNS_Live_StoreMaster.csv')
        self.size_ratio = pd.read_parquet(directory + 'Size_Ratio.parquet')


class InputDataFrames:
    """
    Contains Input DataFrame Objects --> Daily-Inventory and Daily-Sales
    """
    def __init__(self):
        #self.input_path = get_input_path()
        bucket_name = get_value_from_config(config_file, client_config, bucket_name_key)
        tenant = get_value_from_config(config_file, client_config, tenant_key)

        start_date = pd.to_datetime('23-Jan-2020')
        end_date = pd.to_datetime('23-Feb-2020')

        print('Reading Daily Files...')
        self.daily_inventory = get_data_from_date_range(bucket_name, tenant, "daily-store-inventory", start_date, end_date)
        #self.daily_inventory = pd.read_parquet(self.input_path + 'Daily-Inventory.parquet')
        self.daily_inventory = self.daily_inventory.groupby([col_date, col_sku, col_store_code]
                                                            )[col_inv_qty].sum().reset_index().drop_duplicates()
        self.daily_sales = get_data_from_date_range(bucket_name, tenant, "daily-store-sales", start_date, end_date)
        #self.daily_sales = pd.read_parquet(self.input_path + 'Daily-Sales.parquet')
        self.daily_sales = self.daily_sales.groupby([col_date, col_sku, col_store_code]
                                                    )[col_quantity].sum().reset_index().drop_duplicates()

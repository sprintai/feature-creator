from base.impute_method import fillna_with_mean_ag
from base.outlier_removal import get_reliable_data_availability
from input.time import Time

from base.colnames import col_calc_feature_value, col_week, col_year, col_style_code, \
    col_store_code, col_primary_feature, col_technique, col_reliable, col_impute_method, col_n_period_agg, \
    col_start_date
from base.config_params import ros_bucket, availability_bucket, sales_bucket
from base.input_params import is_rel_key_input, is_imp_key_input, imp_method_key_input, technique_key_input, \
    agg_func_key_input

"""
General Checks to be added:
    1. ERROR - Data Present for that time hierarchy?  
    2. WARN - Products/ Stores for which Data Not Present
    3. is_complete TH_Value?
    4. Base Feature Value Description -> Mean, Median, Percentiles, Count, Nans, Shape
    5. WARN Outliers + Removal
    6. Check before combinations should be equal to after feature generation
    
Reliability Checks:
    1. # NaNs before and after Unreliable removal
    2. Base Feature Value Mean, std (Before and After)
    3. Warn if more than 25% of the data is set to NA
    4. Error if more than 50% of data is set to NA

Imputation Checks:
    1. # Nan's Remaining
    2. Base Feature Value Mean, std (Before and After) --> mean after should be within 2 std from mean before  (TBD) 
"""


class FeatureCalculator:
    """
    Base Class for Feature Calculation. Primary feature Classes override its class methods
    """

    def __init__(self, feature, date):
        self.feature = feature
        self.base_df = None
        self.calc_df = None
        self.n_period_agg = self.feature.n_period_agg
        self.date = date
        self.start_date = None
        self.end_date = None

    def get_start_and_end_date(self):
        """
        Gets Start and End Dates for which features need to be calculated
        :return: start_date, end_date
        """
        time = Time(self.date, self.n_period_agg, self.feature.time_hierarchy)
        self.start_date = time.get_start_date()
        self.end_date = time.get_end_date()

        return self.start_date, self.end_date

    def extract_data(self):
        """
        Extracts Relevant Data from Base and Daily Input Files on the basis of start and end Dates
        :return: None
        """
        raise NotImplementedError

    def get_base_calc_feature(self):
        """
        Calculates Base Primary Feature using the extracted data and saves it as base_data
        :return: None
        """
        raise NotImplementedError

    def get_reliable_data(self, df):
        """
        Sets unreliable points in df to N/A
        :param df: DataFrame
        :return: Reliable DataFrame
        """
        if self.feature.primary_feature == ros_bucket:
            return df

        elif self.feature.primary_feature == availability_bucket:
            df = get_reliable_data_availability(df)
            return df

        elif self.feature.primary_feature == sales_bucket:
            return df

    def get_imputed_data(self, df):
        """
        Imputes N/A in df based on Impute method defined in Feature Object
        :param df: DataFrame
        :return: Imputed DataFrame
        """
        if self.feature.calc_method[imp_method_key_input] == 'mean_ag':
            df = fillna_with_mean_ag(df)

        return df

    def add_relevant_columns(self, df):
        """
        Adds Relevant Columns to the Final df
        :param df: DataFrame
        :return: DataFrame with Relevant Columns
        """
        df[col_primary_feature] = self.feature.primary_feature
        df[col_technique] = self.feature.calc_method[technique_key_input]
        df[col_reliable] = self.feature.calc_method[is_rel_key_input]
        df[col_impute_method] = self.feature.calc_method[imp_method_key_input]
        df[col_n_period_agg] = self.n_period_agg
        df[col_start_date] = self.start_date

        time = Time(self.date, self.n_period_agg, self.feature.time_hierarchy)
        df[col_week] = time.week
        df[col_year] = time.year

        return df

    def calc_agg_func(self):
        """
        Aggregates df on the basis of Agg Function defined for Feature (Sum/Mean)
        :return: None
        """
        if self.feature.calc_method[agg_func_key_input] == 'mean':
            print("Agg Func set to Mean")
            self.calc_df = self.calc_df.groupby(self.feature.location_hierarchy \
                                                + self.feature.product_hierarchy)[
                col_calc_feature_value].mean().reset_index()

        elif self.feature.calc_method[agg_func_key_input] == 'sum':
            print("Agg Func set to Sum")
            self.calc_df = self.calc_df.groupby(self.feature.location_hierarchy \
                                                + self.feature.product_hierarchy)[
                col_calc_feature_value].sum().reset_index()

    def calculate_feature(self):
        """
        Calculates Feature with the following steps-
        1. Calculate Base Feature Value using given technique
        1. Set Unreliable to N/A if is_reliable is set to 1
        2. Impute N/As if is_imputed is set to 1
        3. Drop N/As
        4. Aggregate Base Feature using hierarchies and base Functions
        :return: Final DataFrame
        """

        hierarchy = self.feature.location_hierarchy + self.feature.product_hierarchy \
                    + self.feature.time_hierarchy

        self.base_df = self.base_df.groupby([col_style_code, col_store_code] + \
                                            hierarchy)[col_calc_feature_value].sum().reset_index()

        if self.feature.calc_method[is_rel_key_input] == 1:
            self.calc_df = self.get_reliable_data(self.base_df)
            print('Reliability set to 1, removing outliers..')
        else:
            print('Reliability set to 0, proceeding without removing outliers..')
            self.calc_df = self.base_df

        if self.feature.calc_method[is_imp_key_input] == 1:
            self.calc_df = self.get_imputed_data(self.base_df)
            print('Imputation set to 1, Imputing Nans using: ', self.feature.calc_method[imp_method_key_input])
        else:
            print('Imputation set to 0, proceeding without it..')

        self.calc_df = self.calc_df.loc[self.calc_df[col_calc_feature_value].notna()]
        print('Dropping Nans...')

        self.calc_agg_func()
        print("Shape of Feature: ", self.calc_df.shape)

        calc_feature = self.add_relevant_columns(self.calc_df)

        return calc_feature

from base.colnames import col_calc_feature_value


def fillna_with_mean_ag(df):

    df[col_calc_feature_value] = df.groupby(['AG','Store_Code','Week','Year'])[col_calc_feature_value].transform(lambda x: x.fillna(x.mean()))
    return df
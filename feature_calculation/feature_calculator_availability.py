from base.core import Availability, get_data_from_date_range, merge_and_add_relevant_columns
from feature_calculation.feature_calculator import FeatureCalculator
from base.colnames import col_simple_avl, col_weighted_avl, col_calc_feature_value
from base.input_params import technique_key_input
import pandas as pd


class FeatureCalculatorAvailability(FeatureCalculator):
    """

    """
    def __init__(self, feature, date):
        super().__init__(feature, date)
        self.daily_inventory = None

    def extract_data(self, daily_inventory: pd.DataFrame):
        """
        Extracts Daily Inventory Data for the given Time Hierarchy
        :param daily_inventory: Daily Inventory File
        :return:
        """
        start_date, end_date = self.get_start_and_end_date()

        self.daily_inventory = get_data_from_date_range(daily_inventory, start_date, end_date)

    def get_base_calc_feature(self, product_master: pd.DataFrame, store_master: pd.DataFrame, size_ratio:pd.DataFrame):
        """
        Calculates Base Features for Availability --> Simple and Weighted
        :param product_master:
        :param store_master:
        :param size_ratio:
        :return: None
        """

        availability = Availability(self.daily_inventory, product_master)

        if availability.check_empty():
            self.base_df = None
            return

        print('Calculating Using Technique:', self.feature.calc_method[technique_key_input])
        if self.feature.calc_method[technique_key_input] == 'simple':

            self.base_df = availability.get_simple_availability()
            self.base_df.rename(columns={col_simple_avl: col_calc_feature_value}, inplace=True)
            self.base_df = merge_and_add_relevant_columns(self.base_df, product_master, store_master)

        elif self.feature.calc_method[technique_key_input] == 'weighted':

            self.base_df = availability.get_weighted_availability(size_ratio)
            self.base_df.rename(columns={col_weighted_avl: col_calc_feature_value}, inplace=True)
            self.base_df = merge_and_add_relevant_columns(self.base_df, product_master, store_master)

    def calculate_feature(self, daily_inventory, product_master, store_master, size_ratio):
        """
        Inherits calculate_feature from feature_calculator Class and calculates Availability
        :param daily_inventory:
        :param product_master:
        :param store_master:
        :param size_ratio:
        :return:
        """
        self.extract_data(daily_inventory)
        self.get_base_calc_feature(product_master, store_master, size_ratio)
        if self.base_df is None:
            print("No Calculations Done!")
            return None
        return super().calculate_feature()

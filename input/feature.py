from input.input_parser import get_list_from_config, get_value_from_config

from base.config_params import *
from base.input_params import *


class Feature:
    """
    Feature Object containing all Feature Attributes and Checks
    Feature_id --> Unique ID of the Feature
    Primary Feature --> Main Feature from Primary Feature Bucket. Eg ROS, Sales, Availability
    Method - Contains the details of the Feature Calculation methodology
        is_reliable:  if 1 set unreliable data to N/A ->when is_Rel is set to 1, only reliable data points are considered,
         others are set to N/A, which are then imputed if Is_Imputed is set to 1.
        Technique: Basic Technique of Calculating the Primary Feature. Eg - Simple ROS = Sales/WAVL
        is_imputed: Boolean if 1 replace N/A values with Impute_Method
        Impute_Method: Imputation Method (Eg- Replace value with AG)
        AggFunc: Define the method of aggregating the feature Values. Currently can be mean or sum
    n_period_agg --> number of periods for which the feature needs to be aggregated. Eg (n=4 means Last 4 periods)
    Time Key, PH Key, LH Key - Foreign Key from the Hierarchies Table
    Time Hierarchy, PH Hierarchy, LH Hierarchy - Hierarchies for which Feature Value is Calculated

    """

    def __init__(self, feature):

        self.feature_id = feature[feature_id_key_input]
        self.primary_feature = feature[primary_feature_id_key_input]
        self.calc_method = feature[method_key_input]
        self.n_period_agg = feature[n_period_agg_key_input]
        self.time_key = feature[time_id_key_input]
        self.time_hierarchy = feature[time_hierarchy_key_input]
        self.location_key = feature[loc_id_key_input]
        self.location_hierarchy = feature[loc_hierarchy_key_input]
        self.product_key = feature[prod_id_key_input]
        self.product_hierarchy = feature[prod_hierarchy_key_input]

    @property
    def primary_feature(self):
        return self.__primary_feature

    @primary_feature.setter
    def primary_feature(self, value):
        feature_list = get_list_from_config(config_file, feature_section_config, feature_list_key_config)
        if value not in feature_list:
            raise Exception("Primary Feature not present in Feature Bucket")

        self.__primary_feature = value

    @property
    def calc_method(self):
        return self.__calc_method

    @calc_method.setter
    def calc_method(self, value):
        try:
            section = self.__primary_feature + '_properties'
            method_list = get_list_from_config(config_file, section, method_key_config)
            rel_cal = int(get_value_from_config(config_file, section, rel_cal_key_config))
            impute_method_list = get_list_from_config(config_file, feature_section_config, impute_method_key_config)
        except KeyError as e:
            print("Values not present in config for bucket, setting default values..")
            method_list = ['simple']
            rel_cal = 0
            impute_method_list = []

        if value[technique_key_input] not in method_list:
            raise Exception("Method not defined in Config for Id: " + self.feature_id)
        if int(value[is_rel_key_input]) > rel_cal:
            raise Exception("Reliable Calculation not defined in Config for Id: " + self.feature_id)
        if int(value[is_imp_key_input]) == 1:
            try:
                if value[imp_method_key_input] not in impute_method_list:
                    raise Exception("Impute Method Not Defined for Id: " + self.feature_id)

            except KeyError as e:
                raise Exception("Impute Method Key Not present in Input File for Id: " +  self.feature_id)

        self.__calc_method = value
